use std::error::Error;

use common::Message;
use tokio::{self, io::AsyncWriteExt, net::TcpStream};

#[tokio::main]
async fn main() -> Result<(), Box<dyn Error>> {
    let mut stream = TcpStream::connect("127.0.0.1:12345").await?;
    let msg = Message::Text("Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Tincidunt eget nullam non nisi est. Ultrices vitae auctor eu augue ut lectus arcu bibendum at. Massa placerat duis ultricies lacus sed turpis tincidunt id aliquet. Mauris vitae ultricies leo integer malesuada nunc vel risus. Suscipit adipiscing bibendum est ultricies. Faucibus pulvinar elementum integer enim neque volutpat ac. Tincidunt praesent semper feugiat nibh sed pulvinar proin gravida. Amet nisl purus in mollis nunc sed id semper risus. Maecenas volutpat blandit aliquam etiam erat velit scelerisque in dictum. Nec nam aliquam sem et tortor consequat id porta. Id leo in vitae turpis massa. Vulputate ut pharetra sit amet aliquam id diam. Habitasse platea dictumst quisque sagittis purus. At erat pellentesque adipiscing commodo. Aliquet risus feugiat in ante metus dictum at. Viverra tellus in hac habitasse. Tellus in metus vulputate eu scelerisque felis imperdiet proin fermentum. Pharetra pharetra massa massa ultricies mi quis hendrerit dolor magna. Duis convallis convallis tellus id interdum velit.".to_owned());
    let mut buf: Vec<u8> = vec![0; 2048];
    let writen_bytes = postcard::to_vec(&msg).expect("bufffer smol?");
    stream.write(writen_bytes).await?;
    stream.write(writen_bytes).await?;
    stream.write(writen_bytes).await?;
    stream.write(writen_bytes).await?;
    stream.write(writen_bytes).await?;
    stream.write(writen_bytes).await?;
    stream.write(writen_bytes).await?;
    Ok(())
}