use std::{
    io::{prelude::*, BufReader},
    net::{TcpListener, TcpStream, IpAddr}, collections::HashMap, sync::{Arc, Mutex},
};
use futures::executor::ThreadPool;
use futures::task::SpawnExt;
use env_logger::Env;


const HOST: &str = "127.0.0.1:1234";


#[derive(Debug)]
enum PacktageType {
    CreateID,
    Login,
    Logout,
    Message,
    Unknown,
}

impl TryFrom<usize> for PacktageType {
    type Error = ();

    fn try_from(v: usize) -> Result<Self, Self::Error> {

        match v {
            x if x == PacktageType::CreateID as usize => Ok(PacktageType::CreateID),
            x if x == PacktageType::Login as usize    => Ok(PacktageType::Login),
            x if x == PacktageType::Logout as usize   => Ok(PacktageType::Logout),
            x if x == PacktageType::Message as usize  => Ok(PacktageType::Message),
            _ => Err(()),
        }
    }
}


#[derive(Debug)]
struct Server {
    clients: HashMap<Uuid, IpAddr>,
    pool: ThreadPool,
    listener: TcpListener
}

impl Server {
    fn new(host: &str) -> Self {
        let listener = TcpListener::bind(host).unwrap();
        let cpu_count = num_cpus::get();
        log::info!("Using {} threads.", cpu_count);
        let pool = ThreadPool::builder().pool_size(cpu_count).name_prefix("ConnectionHandler").create().unwrap();
        let clients = HashMap::new();
        
        Self {
            clients,
            pool,
            listener
        }
    }

    fn run(&self) {
        let clients_threaded: Arc<Mutex<HashMap<Uuid, IpAddr>>> = Arc::new(Mutex::new(self.clients));
        for stream_result in self.listener.incoming() {
            match stream_result {
                Ok(stream) => {
                    self.pool.spawn(handle_connection(clients_threaded.clone(), stream)).expect("Could no spawn thread from pool to handle connection.");
                }
                Err(e) => log::error!("Error: {e}"),
            }
        }
    }
}


async fn handle_connection(clients: Arc<Mutex<HashMap<Uuid, IpAddr>>>, mut stream: TcpStream) {
    let ip = if let Ok(addr) = stream.peer_addr() {
        addr.ip().to_string()
    } else {
        "Unknown".to_string()
    };

    let mut buf_reader = BufReader::new(&mut stream);
    
    let mut buf:String = Default::default();

    while !buf_reader.buffer().is_empty() {
        let len = buf_reader.read_line(&mut buf);
    }
    // .take_while(Result::is_ok)
    // .map(|result| result.unwrap())
    // .take_while(|line| !line.contains("<ññcloseññ>"))
    // .collect();

    println!("{ip}: {buf:#?}");

    if buf.is_empty() {return};

    let packtage_type = if let Ok(x) = buf.parse::<usize>() {
        x.try_into().unwrap_or(PacktageType::Unknown)
    } else {
        PacktageType::Unknown
    };

    match packtage_type {
        PacktageType::CreateID => handle_createid(clients, stream, buf),
        PacktageType::Login => handle_login(clients, stream, buf),
        PacktageType::Logout => handle_logout(clients, stream, buf),
        PacktageType::Message => handle_message(clients, stream, buf),
        PacktageType::Unknown => handle_unknown(clients, stream, buf),
    }
}

fn handle_createid(clients: Arc<Mutex<HashMap<Uuid, IpAddr>>>, mut stream: TcpStream, _msg: String) {
    let id = Uuid::new_v4();
    let addr: IpAddr;
    match stream.peer_addr() {
        Ok(a) => addr = a.ip(),
        Err(_) => {
            log::error!("Could not get addr when creating ID");
            return;
        },
    }
    let mut clients = clients.lock().expect("Could not adquire lock for clients in thread");
    clients.insert(id, addr);

    match stream.write_all(format!("{}\n{id}", PacktageType::CreateID as usize).as_bytes()) {
        Ok(_) => (),
        Err(e) => log::error!("Could not write response: {e}"),
    }
}
fn handle_login(clients: Arc<Mutex<HashMap<Uuid, IpAddr>>>, mut stream: TcpStream, msg: String) {
    let mut clients = clients.lock().expect("Could not adquire lock for clients in thread");
    let id = msg.lines().collect::<Vec<&str>>()[0];
    let addr: IpAddr;
    match stream.peer_addr() {
        Ok(a) => addr = a.ip(),
        Err(_) => {
            log::error!("Could not get addr when creating ID");
            return;
        },
    }
    match Uuid::parse_str(id) {
        Ok(uuid) => clients.insert(uuid, addr),
        Err(_) => return,
    };
    match stream.write_all(format!("{}\n", PacktageType::Login as usize).as_bytes()) {
        Ok(_) => (),
        Err(e) => log::error!("Could not write response because {e}"),
    }
}

fn handle_logout(clients: Arc<Mutex<HashMap<Uuid, IpAddr>>>, mut stream: TcpStream, msg: String) {
    let mut clients = clients.lock().expect("Could not adquire lock for clients in thread");
    let id = msg.lines().collect::<Vec<&str>>()[0];
    let addr: IpAddr;
    match stream.peer_addr() {
        Ok(a) => addr = a.ip(),
        Err(_) => {
            log::error!("Could not get addr when creating ID");
            return;
        },
    }
}
fn handle_message(clients: Arc<Mutex<HashMap<Uuid, IpAddr>>>, mut stream: TcpStream, msg: String) {
    todo!()
}
fn handle_unknown(clients: Arc<Mutex<HashMap<Uuid, IpAddr>>>, mut stream: TcpStream, msg: String) {
    return;
}

fn main() {
    let env = Env::new()
        .filter_or("AGCHATSV_LOG", "info");

    env_logger::Builder::from_env(env).format_target(false).init();
    
    let server = Server::new(HOST);
    server.run();
}