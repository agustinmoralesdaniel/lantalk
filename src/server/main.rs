use std::{env, error::Error, io::Cursor};

use common::Message;
use tokio::{self, io::{AsyncReadExt, AsyncWriteExt}, net::TcpListener};

// We need a request handler: in this case we implement a simple
// "Hello, World" handler.
async fn handle(msg: Message) -> Result<(), Box<dyn Error>> {
    match msg {
        Message::Text(txt) => text(txt),
        Message::Login(alias) => login(alias),
        Message::Logout => todo!(),
        Message::VideoStreamStart => todo!(),
        Message::VideoStreamFinish => todo!(),
        Message::VideoStremJoin(alias) => todo!(),
        Message::VideoStreamDisc(alias) => todo!(),
        Message::VoiceChatJoin => todo!(),
        Message::VoiceChatDisc => todo!(),
        Message::OK => todo!(),
        Message::Wrong => todo!(),
    }
}

fn text(msg: String) -> Result<(), Box<dyn Error>> {
    println!("{:?}", msg);
    todo!()
}
fn login(alias: String) -> Result<(), Box<dyn Error>> {
    todo!()
}
fn logout() -> Result<(), Box<dyn Error>> {
    todo!()
}
fn video_stream_start() -> Result<(), Box<dyn Error>> {
    todo!()
}
fn video_stream_finish() -> Result<(), Box<dyn Error>> {
    todo!()
}
fn video_strem_join() -> Result<(), Box<dyn Error>> {
    todo!()
}
fn video_stream_disc() -> Result<(), Box<dyn Error>> {
    todo!()
}
fn voice_chat_join() -> Result<(), Box<dyn Error>> {
    todo!()
}
fn voice_chat_disc() -> Result<(), Box<dyn Error>> {
    todo!()
}

// fn get_db() {
//     static DB = Arc<Mutex<HashMap<
// }

#[tokio::main]
async fn main() -> Result<(), Box<dyn Error>> {
    env_logger::Builder::new().filter_level(log::LevelFilter::Info).init();

    let addr = env::args()
        .nth(1)
        .unwrap_or_else(|| "127.0.0.1:12345".to_string());
    let listener: TcpListener = TcpListener::bind(&addr).await?;
    log::info!("Listening on: {}", addr);

    loop {
        let (mut socket, addr) = listener.accept().await?;
        log::info!("Accepting {addr}");
        tokio::spawn(async move {
            let mut buf = vec![0; 2048];
            let mut buff = Cursor::new(vec![0; 2048]);
            loop {
                let n_read = socket
                    .read_buf(&mut buff)
                    .await
                    .expect("failed to read data from socket");

                if n_read == 0 {
                    return;
                }

                loop {
                    let bytes_read: Vec<u8> = (&buf[..n_read]).to_vec();
                    match postcard::take_from_bytes::<Message>(&bytes_read) {
                        Ok((msg, rest)) => {
                            println!("{msg:?}");
                            let mut i = 0;
                            for u in rest {
                                buf.insert(i, u.clone());
                                i += 1;
                            }
                        },
                        Err(e) => {
                            log::error!("{e:?}");
                            break;
                        },
                    };
                    if buf.is_empty() {
                        break;
                    }
                }
                

                // socket
                //     .write_all(&buf[0..n_read])
                //     .await
                //     .expect("failed to write data to socket");
            }
        });
    }
}