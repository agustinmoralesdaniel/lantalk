use std::{thread::Thread, collections::HashMap, net::{IpAddr, TcpListener, TcpStream, SocketAddr}, sync::{Arc, Mutex}, io::{BufReader, BufRead, Read}};
use env_logger::Env;

use common::{Message, Protocol};
use serde::Deserialize;

#[derive(Debug)]
struct Server {
    clients: Arc<Mutex<HashMap<IpAddr, String>>>,
    threads: Vec<Thread>,
    listener: TcpListener,
    should_run: bool
}

impl Server {
    fn new(host: &str) -> Self {
        let listener: TcpListener = TcpListener::bind(host).unwrap();
        log::info!("Listening on {host}");
        let threads = vec![];
        let clients = Arc::new(Mutex::new(HashMap::new()));
        
        Self {
            clients,
            threads,
            listener,
            should_run: false,
        }
    }

    fn run(&mut self) {
        // let clients_threaded: Arc<Mutex<HashMap<IpAddr, String>>>  = self.clients.clone();
        self.should_run = true;
        while self.should_run {
            match self.listener.accept() {
                Ok((stream, _addr)) => self.handle(stream),
                Err(e) => log::error!("Error accepting connection: {e:?}"),
            }
        }
    }

    fn handle(&self, mut stream: TcpStream) {
        let clients = self.clients.clone();
        let ip = stream.peer_addr().expect("Stream has peer_addr");
        log::info!("{ip} connecting...");
        let mut protocol = Protocol::with_stream(stream)?;

        loop {
            match Message::deserialize(stream) {
                Ok(msg) => {
                    log::info!("{msg}")
                },
                Err(e) => log::error!("Error deserializing message: {e:?}"),
            }
        }
    }
}

fn main() {
    let env = Env::new().filter_or("LANTALK_LOG", "info");
    env_logger::Builder::from_env(env).format_target(false).init();
    
    let mut server = Server::new("0.0.0.0:9999");
    server.run();
}