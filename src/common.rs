use serde::{Deserialize, Serialize};

#[derive(Debug, Serialize, Deserialize)]
pub enum Message {
    Text(String),
    Login(String),
    Logout,
    VideoStreamStart,
    VideoStreamFinish,
    VideoStremJoin(String),
    VideoStreamDisc(String),
    VoiceChatJoin,
    VoiceChatDisc,
    OK,
    Wrong,
}
